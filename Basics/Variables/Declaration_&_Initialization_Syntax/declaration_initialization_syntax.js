/* ANCHOR INTRODUCTION
 * The following file covers declaration
 * and initialization syntax.
 * 
 * For demonstration purposes and to avoid confusion,
 * a new variable will be created for each example. 
 * This is not typical or necessary when working with 
 * the same variable and especially not when declaring and
 * initializing a variable on separate lines.
 * 
 * Certain lines have been marked out with "//" 
 * so they are not run by the console.
 * 
 */

/* -------------------------------------------------------------------------- */


/**
 * Declaration
 * 
 * To declare a variable start with a declarative
 * keyword followed by whatever the variable name is.
 * Declaration and all other single lines of code end
 * with a semi-colon.
 * 
 * Note: The variable name can be anything but it should be
 * something related to what the value is or is going to be.
 * There are specific reccomendations for naming variables,
 * but that will be covered in variable_naming_rules.js.
 */

let myFirstStringVariable;


/* 

Because the variable has not been initialized,
the value is 'undefined', meaning it doesn't
have a value.

To see this, remove the "//" from the line below,
save the file, open the variables index.html file
in chrome if not done so already, refresh the page,
inspect the page, and view the console. The variable
value should be present in the console
*/

// console.log(myFirstStringVariable);




/* -------------------------------------------------------------------------- */

/**
 * Initialization
 * 
 * To initialize a variable is to assign a value 
 * to a variable that does not yet have a value.
 * 
 * Because the variable has already been declared
 * the declarative keyword, 'let', does not need
 * to be typed again.
 * 
 * Instead, the variable just needs a value assigned to it.
 * This is done by using the '=' to set the variable
 * equal to whatever value follows the '='.
 * 
 * In the case below the variable is set equal to the 
 * value of a string. 
 * 
 * Note: A string is a variable type that
 * represents text. Strings are encased by quotations.
 * 
 */

let mySecondStringVariable;
mySecondStringVariable = 'A string';

/* 

Since the variable has now been initialized, the variable 
value is a string equal to 'A string'.

To see this, remove the "//" from the line below,
save the file, open the variables index.html file
in chrome if not done so already, refresh the page,
inspect the page, and view the console. The variable
value should be present in the console.
*/

// console.log(mySecondStringVariable);


/* -------------------------------------------------------------------------- */

/**
 * Declaration & Initialization in The Same Line
 * 
 * Variables can also be declared and initialized at the same time. 
 * 
 */

let myThirdStringVariable = 'A string';

/* 

Since the variable has both been declared and initialized 
initialized in the same line, the variable 
value is a string equal to 'A string'.

To see this, remove the "//" from the line below,
save the file, open the variables index.html file
in chrome if not done so already, refresh the page,
inspect the page, and view the console. The variable
value should be present in the console.
*/

// console.log(myThirdStringVariable);


/* -------------------------------------------------------------------------- */
