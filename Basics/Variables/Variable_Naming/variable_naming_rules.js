/* ANCHOR INTRODUCTION
 * The following file covers reccomended rules
 * to follow when naming a variable.
 * 
 * Certain lines have been marked out with "//" 
 * so they are not run by the console.
 * 
 */

/* -------------------------------------------------------------------------- */


/*
 * Use Latin Characters
 * 
 * Use characters, 0-9, a-z, A-Z, and 
 * the underscore character ( _ ).
 */

/* -------------------------------------------------------------------------- */

/*
 * Do not use underscores at the beginning of your variable names
 */

/* 

/* -------------------------------------------------------------------------- */

/*
 * Do not start variable names with numbers
 */

/* -------------------------------------------------------------------------- */

/*
 * Try to use camel case whenever possible when variables are
 * comprised of multiple words.
 */

/* -------------------------------------------------------------------------- */

/*
 * Do not use spaces in variables with names comprised of 
 * multiple words
 */

/* -------------------------------------------------------------------------- */

/*
 * Variable names should be intuitive or obvious, 
 * meaning you should be able to tell what the value 
 * of the variable is just by looking at the name (e.g. 
 * a variable with the name, ‘name’, will most likely hold 
 * a value that equates to someone’s name.
 */

/* -------------------------------------------------------------------------- */

/*
 * Do not use Javascript reserved keywords like, ‘var, function, let, const, for’, and so on.
 */

/* -------------------------------------------------------------------------- */
