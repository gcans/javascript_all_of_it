/* ANCHOR INTRODUCTION
 * The following file covers declaritive
 * keywords and should be completed using
 * the declarative keywords section in
 * the index.html file that cooresponds
 * with the Variables topic.
 */

/* -------------------------------------------------------------------------- */




// ANCHOR THE KEYWORDS

/* -------------------------------------------------------------------------- */

/**
 * var
 * 
 * Classic way of declaring a variable that can be changed.
 * 
 * The var declaration keyword is the 
 * original declarative keyword. Using 
 * only var to declare variables in a 
 * project is no longer considered best
 * practice. 
 */

// HOW TO DECLARE A VARIABLE WITH var
var varVariable;




/* -------------------------------------------------------------------------- */

/**
 * let
 * 
 * Declares a variable that only exists 
 * within the corresponding block. Variables 
 * with the let declaration can be changed. 
 * This is referred to as being, mutable.
 * 
 * This is similar to the var keyword in the 
 * sense that the value can be changed. It
 * differs in where the variable can be used.
 * 
 */

let letVariable;


/* -------------------------------------------------------------------------- */

/**
 * const
 * 
 * Declares a variable that only exists in 
 * the corresponding block. Variables with the 
 * const declaration cannot be changed. This 
 * is referred to as being, immutable.
 * 
 */

const constVariable;


/* -------------------------------------------------------------------------- */





// ANCHOR WHEN TO USE THE KEYWORDS

/* -------------------------------------------------------------------------- */
/**
 * var
 * 
 * Classic way of declaring a variable that can be changed.
 * 
 * The var declaration keyword is the 
 * original declarative keyword. Using 
 * only var to declare variables in a 
 * project is no longer considered best
 * practice. 
 */

// HOW TO DECLARE A VARIABLE WITH var
var varVariable;




/* -------------------------------------------------------------------------- */

/**
 * let
 * 
 * Declares a variable that only exists 
 * within the corresponding block. Variables 
 * with the let declaration can be changed. 
 * This is referred to as being, mutable.
 * 
 * This is similar to the var keyword in the 
 * sense that the value can be changed. It
 * differs in where the variable can be used.
 * 
 */

let letVariable;


/* -------------------------------------------------------------------------- */

/**
 * const
 * 
 * Declares a variable that only exists in 
 * the corresponding block. Variables with the 
 * const declaration cannot be changed. This 
 * is referred to as being, immutable.
 * 
 */

const constVariable;


/* -------------------------------------------------------------------------- */